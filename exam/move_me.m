function R= move_me(A)
t=input('t=');
R = t*ones(size(A));
c=A~=t; 
R(6-sum(c):end) = A(c);
end
